//Load the required libs
const path = require("path");
const express = require("express");

//create an instance of the app
const app = express();

//List of handlers
//GET /time
app.get("/time", function(req, resp) {
    resp.status(200);
    resp.type("text/html");
    resp.send("<h1>" + new Date() + "</h1>");
});

//GET files in client folder
//app.use(express.static(__dirname + "../client"));
app.use(
    express.static(path.join(__dirname, "../client"))
);

app.use(function(req, resp) {
    resp.type("text/html");
    resp.status(404);
    resp.send("Looks like your file is not here");
});

//configure the server
const port = process.env.APP_PORT || 3000;

//ES6
//app.listen(port, () => { });
app.listen(port, function() {
    console.log("Application started on port %d", port);
    console.log("app path: %s", __dirname);
});