(function() {
	var CustomerApp = angular.module("CustomerApp", []);

	var CustomerSvc = function($http, $q) {
		var customerSvc = this;

		customerSvc.findAllCustomers = function() {
			var defer = $q.defer();

			$http.get("/customers")
				.then(function(result) {
					defer.resolve(result.data);

				}).catch(function(err) {
					defer.reject(err);
				});

			return (defer.promise);
		}

		customerSvc.findCustomerById = function(cid) {
			var defer = $q.defer();

			$http.get("/customer/" + cid)
				.then(function(result) {
					defer.resolve(result.data);

				}).catch(function(err) {
					defer.reject(err);
				});

			return (defer.promise);
		}
	}
	CustomerSvc.$inject = ["$http", "$q"];

	var CustomerCtrl = function(CustomerSvc) {
		var customerCtrl = this;

		customerCtrl.customers = [];
		customerCtrl.customer = {};

		customerCtrl.getDetails = function(customerId) {
			console.log(">> customer id = %d", customerId);
			CustomerSvc.findCustomerById(customerId)
				.then(function(result) {
					console.info(">> customer = ", result);
					customerCtrl.customer = result;

				}).catch(function(err) {
					console.error("err = ", err);
				})
		}

		CustomerSvc.findAllCustomers()
			.then(function(result) {
				console.info(">>> customers = ", result);
				customerCtrl.customers = result;

			}).catch(function(err) {
				console.error("err = ", err);
			});
	}
	CustomerCtrl.$inject = [ "CustomerSvc" ];

	CustomerApp.service("CustomerSvc", CustomerSvc);
	CustomerApp.controller("CustomerCtrl", CustomerCtrl);

})();
