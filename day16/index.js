(function() {
    var MyApp = angular.module("MyApp", ["ui.router"]);

    //Define a config for my app
    var MyConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("home", {
                url: "/home",
                templateUrl: "/views/home.html"
            })
            .state("dog", {
                url: "/dog",
                templateUrl: "/views/dog.html"
            })
            .state("cat", {
                url: "/cat",
                templateUrl: "/views/cat.html",
                controller: "CatCtrl as catCtrl" //ng-controller
                /*
                controller: CatCtrl,
                controllerAs: "catCtrl" */
            })

        $urlRouterProvider.otherwise("/home");
    }
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    MyApp.config(MyConfig);

    var CatCtrl = function($state) {
        console.info("Initializing CatCtrl");
        var catCtrl = this;
        catCtrl.back = function() {
            console.info(">>> back!");
            $state.go("home");
        }
    }
    CatCtrl.$inject = [ "$state" ]; //$state is provided by ui.router
    var MyCtrl = function() {
        var myCtrl = this;
    }

    MyApp.controller("MyCtrl", MyCtrl)
    MyApp.controller("CatCtrl", CatCtrl);

})();