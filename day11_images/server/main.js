//Load libraries
const path = require("path");
const express = require("express");

const images = ['dog0.png', 'dog1.png'];

const randomImg = function(range) {
    if (!range)
        range = 12;
    var num = Math.floor(Math.random() * range);
    return ("dog" + num + ".jpg");
}

//Create an instance of express app
const app = express();

//Define rules to process requrest
//based on (method, resource)

app.get("/", function(req, resp){
    const image = path.join("/c/channel", randomImg());
    resp.status(200);
    resp.type("text/html");
    resp.send("<img src='" + image + "'>");
});

app.get("/images", function(req, resp) {
    var imageName = randomImg();
    resp.status(200);
    resp.type("image/gif");
    resp.sendFile(path.join(
        __dirname, "../assets/images", imageName));
});

app.use("/c/channel", 
    express.static(path.join(__dirname, "../assets/images")));

app.use("/", express.static(path.join(__dirname, "../client")));

//config the server
const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
    console.info("Application started at %s on port %d"
            , new Date(), port);
});