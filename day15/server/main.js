//Load the libraries
const q = require("q");
const path = require("path");
const mysql = require("mysql");
const express =require ("express");

//config database
const sampledb = mysql.createPool({
	host: "localhost", port: 3306,
	user: "fred", password: "fred",
	database: "derby_sample",
	connectionLimit: 5
});

const mkQuery = function(sql, pool) {
	return (function(/* some params */) {
		//Create a defer because mysql query will be
		//async
		const defer = q.defer();
		//
		//Collect the arguments
		var params = [];
		for (var i in arguments)
			params.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			//Check if we have gotten a connection
			if (err) {
				//If there are any errors, reject the promise
				defer.reject(err);
				return;
			}

			//Make the query
			conn.query(sql, params, function(err, result) {
				if (err)
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});
		//Return the promise that is associated with the defer 
		return (defer.promise);
	});
}

const SELECT_ALL_CUSTOMERS = "select NAME, CUSTOMER_ID from CUSTOMER";
const SELECT_CUSTOMERS_BY_ID = 
	"select * from CUSTOMER where CUSTOMER_ID = ?";

const findAllCustomers = mkQuery(SELECT_ALL_CUSTOMERS, sampledb);
const findCustomerById = mkQuery(SELECT_CUSTOMERS_BY_ID, sampledb);

//Create an instance of express
const app = express();

//Config the routes
app.get("/customers", function(req, resp) {
	findAllCustomers()
		.then(function(result) {
			resp.status(200);
			resp.type("application/json");
			resp.json(result); 

		}).catch(function(err) {
			resp.status(500);
			resp.type("application/json");
			resp.json(err);
		});
});

app.get("/customer/:cid", function(req, resp) {
	const customerId = parseInt(req.params.cid);
	findCustomerById(customerId)
		.then(function(result) {
			//result [{ }]
			if (result.length == 0) {
				resp.status(404);
				return;
			}

			resp.status(200);
			resp.format({
				"application/json": function() {
					resp.json(result[0]);
				},
				"text/csv": function() {
					//Loop thru all the values then
					//create a CSV using .join()
					var vals = [];
					const rec = result[0];
					for (var i in rec)
						vals.push(rec[i]);
					resp.send(vals.join(","));
				},
				"default": function() {
					resp.status(406);
				}
			});

		}).catch(function(err) {
			resp.status(500);
			resp.type("application/json");
			resp.json(err);
		});
});

//Map / -> client
app.use(express.static(path.join(__dirname, "../client")));

//Map /libs -> /bower_components
app.use("/libs", 
	express.static(path.join(__dirname, "../bower_components")));

//Config the server
console.log("STATIC_RES = %s", process.env.STATIC_RES);

const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Application started at %s on port %d"
			, new Date(), port);
});
