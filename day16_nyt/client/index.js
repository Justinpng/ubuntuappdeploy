(function() {
	var NYTBookApp = angular.module("NYTBookApp", ["ui.router"]);

	//Configure the book
	var NYTBookConfig = function($stateProvider, $urlRouterProvider) {
		$stateProvider.state("overview", {
			url: "/overview",
			templateUrl: "/views/overview.html",
			controller: "OverviewCtrl as overviewCtrl"
		});

		$urlRouterProvider.otherwise("/overview");
	}
	NYTBookConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];

	//Declare a service
	var NYTBookSvc = function($http, $q) {
		var nytBookSvc = this;

		var nytURL = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
		var nytApiKey = "b8c563c6e5a54692841b8246ccf0924c";

		var idbURL = "https://idreambooks.com/api/books/reviews.json";
		var idbApiKey = "dcab0f326f83548819fb7fd22f909181c1d1faf8";

		nytBookSvc.overview = function() {
			var defer = $q.defer()
			$http.get(nytURL, {
				params: { "api-key": nytApiKey }

			}).then(function(result) {
				var filtered = result.data.results.lists
					.filter(function(category) {
						var listName = category.list_name_encoded;
						return ((listName === "hardcover-fiction") 
							||  (listName === "trade-fiction-paperback"));
					})
				//defer.resolve(filtered);
				defer.resolve(result.data.results.lists);

			}).catch(function(err) {
				defer.reject(err);
			})
			return (defer.promise);
		}

		nytBookSvc.review = function(isbn) {
			var defer = $q.defer();

			$http.get(idbURL, {
				key: idbApiKey,
				q: isbn
			}).then(function(result) {
				if (result.data.total_results > 1)
					defer.resolve(result.data.book);
				else
					defer.resolve({});

			}).catch(function(err) {
				defer.reject(err);
			})

			return (defer.promise);
		}

	}
	NYTBookSvc.$inject = [ "$http", "$q" ]

	//Controller
	var OverviewCtrl = function($state, NYTBookSvc) {
		var overviewCtrl = this;

		overviewCtrl.categories = [];

		overviewCtrl.review = function(isbn13) {
		}

		NYTBookSvc.overview()
			.then(function(categories) {
				overviewCtrl.categories = categories;
			})
	}
	OverviewCtrl.$inject = [ "$state", "NYTBookSvc" ]

	NYTBookApp.config(NYTBookConfig);
	NYTBookApp.service("NYTBookSvc", NYTBookSvc);

	NYTBookApp.controller("OverviewCtrl", OverviewCtrl);

})();
