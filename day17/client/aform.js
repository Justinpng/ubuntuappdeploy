(function() {
	var AFormApp = angular.module("AFormApp", []);

	//AFormSvc.$inject = [ "$http", "$httpParamSerializerJQLike", "$q" ];
	var AFormSvc = function($http, $httpParamSerializerJQLike, $q) {
		var aFormSvc = this;

		aFormSvc.postAsJSON = function(url, body) {
			var defer = $q.defer();
			$http.post(url, body)
				.then(function(result) {
					defer.resolve(result);
				}).catch(function(err) {
					defer.reject(err);
				})
			return (defer.promise);
		}

		aFormSvc.postAsForm = function(u, body) {
			var defer = $q.defer();
			$http({
				method: "POST",
				url: u
				headers: {
					"Content-Type" : "application/x-www-form-urlencoded"
				},
				data: $httpParamSerializerJQLike(body)

			}).then(function(result) {
				defer.resolve(result));

			}).catch(function(err) {
				defer.reject(err);
			});
			return (defer.promise);
		}
	}
	AFormSvc.$inject = [ "$http", "$httpParamSerializerJQLike", "$q" ];

	var AFormCtrl = function(AFormSvc) {
		var aFormCtrl = this;
		aFormCtrl.name = "";
		aFormCtrl.email = "";

		aFormCtrl.register = function() {
			console.log("name = %s", aFormCtrl.name);
			console.log("email = %s", aFormCtrl.email);

			AFormSvc.postAsForm("/register", 
				{ name: aFormCtrl.name, email: aFormCtrl.email })

				.then(function(result) {
					aFormCtrl.name = "";
					aFormCtrl.email = "";
				}):

	}
	//AFormCtrl.$inject = [ "$http" , "$httpParamSerializerJQLike" ];
	AFormCtrl.$inject = [ "AFormSvc" ];

	AFormApp.service("AFormSvc", AFormSvc);
	AFormApp.controller("AFormCtrl", AFormCtrl);
})();
