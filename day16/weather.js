(function() {

     var WeatherApp = angular.module("WeatherApp", ["ui.router"]);

     var WeatherConfig = function($stateProvider, $urlRouterProvider) {

         $stateProvider
            .state("cities", {
                url: "/cities",
                templateUrl: "/views/cities.html",
                controller: "CitiesCtrl as citiesCtrl"
            })
            .state("wdetails", {
                url: "/wdetails/:city",
                templateUrl: "/views/wdetails.html",
                controller: "WDetails as wDetails"
            })

         $urlRouterProvider.otherwise("/cities");

     }
     WeatherConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];

     var WeatherSvc = function($http, $q) {
         var weatherSvc = this;

         weatherSvc.getWeather = function(city, key) {
             var defer = $q.defer();
             $http.get("http://api.openweathermap.org/data/2.5/weather", {
                 params: {
                     q: city,
                     appid: key
                 }
             }).then(function(result) {
                 defer.resolve(result.data.weather);
             }).catch(function(err) {
                 defer.reject(err);
             })
             return (defer.promise);
         }
     }
     WeatherSvc.$inject = [ "$http", "$q" ]

     var WeatherCtrl = function() {
         var weatherCtrl = this;
     }
     var CitiesCtrl = function($state) {
         var citiesCtrl = this;
         citiesCtrl.cities = ["Singapore", "Hong Kong", "Kuala Lumpur", 
            "Bangkok", "Hanoi", "Sydney", "Tokyo", "Belarus", "Prague", "London"];
         citiesCtrl.weatherDetails = function(c) {
             console.info(">>>> city = ", c);
             $state.go("wdetails", { city: c })
         }
     }
     CitiesCtrl.$inject = [ "$state" ]

     var WDetails = function($stateParams, WeatherSvc) {
         var wDetails = this;
         wDetails.city = $stateParams.city
         wDetails.weather = [];
         WeatherSvc.getWeather(wDetails.city, 
                "__API_KEY_HERE__")
            .then(function(weather) {
                wDetails.weather = weather;
                console.log(">> weather: ", weather);
            }).catch(function(err) {
                console.error(">>> getWeather: ", err);
            })
     }
     WDetails.$inject = [ "$stateParams", "WeatherSvc"]


     WeatherApp.config(WeatherConfig);

     WeatherApp.service("WeatherSvc", WeatherSvc);

     WeatherApp.controller("WeatherCtrl", WeatherCtrl);
     WeatherApp.controller("CitiesCtrl", CitiesCtrl);
     WeatherApp.controller("WDetails", WDetails);

})();
