//Load libs
const path = require("path");
const mysql = require("mysql");
const express = require("express");

//Create a connection pool
const pool = mysql.createPool({
    host: "localhost", port: 3306, //Server
    user: "fred", password: "fred",
    database: "mydb",
    connectionLimit: 4
});

/* 
    insert into registrations(email, username, phone)
        values("fred@gmail.com", "Fred", "555-12345")
*/

const SQL_INSERT_REGISTRATION = 
    " insert into registrations(email, username, phone) values(?, ?, ?)";

//Create an instance of the app
const app = express();

//Request handlers

app.get("/register", function(req, resp) {

    console.info("username = %s", req.query.username);
    console.info("email = %s", req.query.email);
    console.info("phone = %s", req.query.phone);

    pool.getConnection(function(err, conn) {
        //Connection pool error
        if (err) {
            console.error(">>>> error: ", err);
            resp.status(500);
            resp.end(JSON.stringify(err));
            return;
        }
        //We have a connection
        conn.query(SQL_INSERT_REGISTRATION
            , [ req.query.email, req.query.username, req.query.phone ]
            , function(err, result) {
                if (err) {
                    console.error(">>>> insert: ", err);
                    resp.status(500);
                    resp.end(JSON.stringify(err));
                    conn.release();
                    return;
                }
            });
        
        conn.release();
        resp.status(200);
        resp.type("text/html");
        resp.send("<h2>Registered</h2>");
    })


});

app.use(express.static(path.join(__dirname, "../client")));

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

//config server
const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
    console.info("Application started at %s on port %d"
            , new Date(), port);
});
