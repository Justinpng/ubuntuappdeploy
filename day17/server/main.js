const path = require("path");
const express = require("express");

const bodyParser = require("body-parser");

const app = express();

//Routes processing cycle
//

// Method: POST
// Content-Type: application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({extended: false}));

app.post("/register", 

	// Content-Type: application/x-www-form-urlencoded
	bodyParser.urlencoded({extended: false}),
	//
	// Content-Type: application/json
	bodyParser.json(),

	function(req, resp) {
		const name = req.body.name;
		const email = req.body.email;
		const trackme = req.body.trackme;

		console.log("name = %s, email = %s", name, email);
		console.log("\ttrackme = %s", trackme);

		resp.status(201);
		resp.type("text/plain");
		resp.send("We have registered your interest. Please check your email: " + email);

	}
);

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

app.use(express.static(path.join(__dirname, "../client")));

const port = 3000;

app.listen(port, function() {
	console.info("Application started on port %d", port);
});
