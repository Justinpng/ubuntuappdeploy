var mkPerson = function(n, e, p, a) {
    return ({ //Instantiate an object and return that
        name: n,
        email: e,
        phone: p,
        age: a,
        hello: function() {
            return "Hello my name is " + this.name
        },
        changeMyName: function(newName) {
            this.name = newName;
        }
    });
}

var Person = function(n, e, p, a) {
    var ctrl = this;
    //Instance variables
    ctrl.name = n;
    ctrl.email = e;
    ctrl.phone = p;
    ctrl.age = a;
}
//Prototypical inheritance
Person.prototype.hello = function() {
    console.log(
        "Hello my name is %s. What is yours?", this.name);
}

//function constructor
var wilma = new Person("Wilma", "wilma@gmail.com", "555-5678", 20);
var betty = new Person("Betty", "betty@bedrock.com", "555-1234", 20);

var barney = mkPerson(
    "barney", "barney@gmail.com", "555-3456", 50);

console.log("------ before");
wilma.hello();

Person.prototype.hello = function() {
    console.log("Hello I'm %s", this.name);
}
Person.prototype.whatIsYourAge = function() {
    console.log("Forever 21!");
}

console.log("------ after");
wilma.hello = function(name) {
    console.log("Hello %s. My name is %s", name, this.name);;
}
wilma.shout = function() {
    console.log("HELLO!!!");
}
wilma.hello("Fred");
wilma.shout();
betty.hello();
betty.whatIsYourAge();
