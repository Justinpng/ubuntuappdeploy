//Load libraries
const path = require("path");
const q = require("q");
const express = require("express");
const mysql = require("mysql");

//Configure MySQL
const pool = mysql.createPool({
	host: "localhost", port: 3306,
	user: "fred", password: "fred",
	database: "sakila",
	connectionLimit: 4
});

//use q from promise
const mkQQuery = function(sql, connPool) {

	const sqlQuery = function() {
		const defer = q.defer();

		console.log("Using q package");
		//
		//Collect the arguments
		var sqlParams = [];
		for (var i in arguments)
			sqlParams.push(arguments[i])

		connPool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}
			conn.query(sql, sqlParams, function(err, result) {
				if (err)
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			})
		});

		return (defer.promise);
	}

	return (sqlQuery);
}

//Native promise
const mkQuery = function(sql, connPool) {
	const sqlQuery = function() {

		//Collect the arguments
		var sqlParams = [];
		for (var i in arguments)
			sqlParams.push(arguments[i])

		const promise = new Promise(function(resolve, reject) {
			connPool.getConnection(function(err, conn) {
				if (err) {
					reject(err);
					return;
				}
				conn.query(sql, sqlParams, function(err, result) {
					if (err)
						reject(err);
					else
						resolve(result);
					conn.release();
				})
			});
		});
		return (promise);
	}
	return (sqlQuery);
}

//Create an instance of express
const app = express();

//List of SQL 
const SELECT_FILM_BY_FILM_ID = "select * from film where film_id = ?";

const findFilmByFilmId = mkQQuery(SELECT_FILM_BY_FILM_ID, pool);

const handleError = function(err, resp) {
	resp.status(500);
	resp.type("application/json");
	resp.json(err);
}

//Add routes to the app
// /film/1 filmId == 1
app.get("/film/:filmId", function(req, resp) {
	//Based on the type, convert appropriately
	const filmId = parseInt(req.params.filmId);

	console.log("filmId = ", filmId);
	//Request a connection from the mysql pool
	if (isNaN(filmId)) {
		resp.status(400);
		resp.end(); //send(), json(),
		return;
	}

	findFilmByFilmId(filmId)
		.then(function(result) {

			if (result.length == 0) {
				resp.status(404);
				resp.end();
				return;
			}

			const film = result[0];
			resp.status(200);
			resp.format({
				"application/json": 
					function() {
						resp.json(film);
					},
				"text/plain": 
					function() {
						const str = "Id: " + film.film_id
							+ "\nTitle: " + film.title 
							+ "\nDescription: " + film.description 
							+ "\nRelease year: " + film.release_year;

						resp.send(str);
					},
				"text/html":
					function() {
						const str = "<h3>Id: </h3>" + film.film_id
							+ "<h3>Title: </h3>" + film.title 
							+ "<h3>Description: </h3>" + film.description 
							+ "<h3>Release year: </h3>" + film.release_year;
						resp.send(str);
					},
				"default":
					function() {
						resp.status(406);
						resp.send("Not acceptable");

					}
			});
		}).catch(function(err) {
			handleError(err, resp);
		});
	}
);

app.get("/film/:category/:filmId", function(req, resp) {
	console.log(">> filmId = ", req.params.filmId)
	console.log(">> category = ", req.params.category)
	resp.end();

});

app.get("/films", function(req, resp) {
});

//Static routes
app.use(express.static(path.join(__dirname, "../client")));

//Configure the server
const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Application started on port %d", port);
});
