(function() {
	var CustomerApp = angular.module("CustomerApp", ["ui.router"]);

	var CustomerConfig = function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("main", {
				url: "/main",
				templateUrl: "/views/main.html",
				controller: "MainCtrl as mainCtrl"

			})
			.state("search", {
				url: "/search/:term",
				templateUrl: "/views/search.html",
				controller: "SearchCtrl as searchCtrl"
			});

		$urlRouterProvider.otherwise("/main");
	}
	CustomerConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ]

	var CustomerSvc = function($http, $q) {
		var customerSvc = this;

		customerSvc.findCustomerByName = function(name) {
			var defer = $q.defer();
			$http.post("/api/customer", { search: name })
				.then(function(result) {
						defer.resolve(result.data);
					})
				.catch(function(err) {
						defer.reject();
					});
			return (defer.promise);
		}

		customerSvc.findPurchaseOrderByCustomerId = function(cid) {
			var defer = $q.defer();
			$http.get("/api/customer/" + cid + "/purchaseorder")
				.then(function(result) {
					defer.resolve(result.data);
				}).catch(function(err) {
					defer.reject(err);
				});
			return (defer.promise);
		};
	}
	CustomerSvc.$inject = [ "$http", "$q" ];

	var MainCtrl = function($state) {
		var mainCtrl = this;
		mainCtrl.customerName = "";
		mainCtrl.search = function() {
			console.info(">>> customer name = %s", mainCtrl.customerName);
			$state.go("search", { term: mainCtrl.customerName });
		}
	}
	MainCtrl.$inject = [ "$state" ]

	var SearchCtrl = function($stateParams, $state, CustomerSvc) {
		var searchCtrl = this;

		searchCtrl.customer = { };
		searchCtrl.purchaseOrder = [];

		searchCtrl.back = function() {
			$state.go("main");
		}

		CustomerSvc.findCustomerByName($stateParams.term)
			.then(function(customer) {
				searchCtrl.customer = customer;
				//If we return customer, customer will be placed in a Promise
				//If we return a promise, then the promise is pass on
				return ( CustomerSvc.findPurchaseOrderByCustomerId(customer.CUSTOMER_ID));
				}

			).then(function(purchaseOrder) {
				searchCtrl.purchaseOrder = purchaseOrder;
				console.log("po = ", purchaseOrder);

			}).catch(function(err) {
				console.error("Error: ", err);
			})

		/*
		CustomerSvc.findCustomerByName($stateParams.term)
			.then(function(customer) {
				searchCtrl.customer = customer;

				CustomerSvc.findPurchaseOrderByCustomerId(customer.CUSTOMER_ID)
					.then(function(purchaseOrder) {
						searchCtrl.purchaseOrder = purchaseOrder;
						console.log("po = ", purchaseOrder);

					}).catch(function(err) {
						console.error("Purchase order error: ", err);
					});

			}).catch(function(err) {
				console.error("Customer %s not found", $stateParams.term);
			});
		*/
	}
	SearchCtrl.$inject = [ "$stateParams", "$state", "CustomerSvc" ]

	CustomerApp.config(CustomerConfig);

	CustomerApp.service("CustomerSvc", CustomerSvc);

	CustomerApp.controller("MainCtrl", MainCtrl);
	CustomerApp.controller("SearchCtrl", SearchCtrl);
})();
