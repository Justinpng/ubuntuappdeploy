const path = require("path");
const express = require("express");

const app = express();

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));
app.use(express.static(path.join(__dirname, "../client")));

const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Application started at %s on port %d"
			, new Date(), port);
});
