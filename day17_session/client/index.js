(function() {
	var CartApp = angular.module("CartApp", []);

	var CartSvc = function($http, $q) {
		var cartSvc = this;

		cartSvc.addToCart = function(item) {
			var defer = $q.defer();

			$http.post("/cart", { payload: item })
				.then(function() {
					defer.resolve();
				});

			return (defer.promise);
		};

		cartSvc.loadFromCart = function() {
			var defer = $q.defer();

			$http.get("/cart")
				.then(function(result) {
					defer.resolve(result.data);
				});

			return (defer.promise);
		}

		cartSvc.checkout = function() {
			var defer = $q.defer();

			$http.delete("/cart")
				.then(function() {
					defer.resolve();
				});

			return (defer.promise);
		}
	};
	CartSvc.$inject = [ "$http", "$q" ];

	var CartCtrl = function(CartSvc) {
		var cartCtrl = this;
		cartCtrl.item = "";
		cartCtrl.content = [];

		cartCtrl.addToCart = function() {
			CartSvc.addToCart(cartCtrl.item)
				.then(function() {
					cartCtrl.content.push(cartCtrl.item);
					cartCtrl.item = "";
				});
		}

		cartCtrl.loadFromCart = function() {
			CartSvc.loadFromCart()
				.then(function(result) {
					cartCtrl.content = result;
				});
		}

		cartCtrl.checkout = function() {
			CartSvc.checkout()
				.then(function() {
					cartCtrl.content = [];
				});
		}

	};
	CartCtrl.$inject = [ "CartSvc" ]

	CartApp.service("CartSvc", CartSvc);
	CartApp.controller("CartCtrl", CartCtrl);

})();
