const uuid = require("uuid/v4");
const path = require("path");
const express = require("express");
const session = require("express-session");

const bodyParser = require("body-parser");

const secretKey = uuid().substring(0, 8);
const sessionConfig = session({
	secret: secretKey,
	resave: false,
	saveUninitialized: true
});

const app = express();

//Routes processing cycle

app.use(sessionConfig);

app.use(function(req, res, next) {
	//new session
	
	if (!req.session.userdata) {
		console.log(">>> creating a new session");
		req.session.userdata = {
			cart: [],
			lastVisit: new Date(),
			history: []
		};
	}

	if (!req.originalUrl.match(/history/))  {
		req.session.userdata.history.push(req.originalUrl);
		req.session.userdata.lastVisit = new Date();
	}

	next();
});

//app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get("/cart", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(req.session.userdata.cart);
});

app.post("/cart", function(req, resp) {
	req.session.userdata.cart.push(req.body.payload);
	resp.status(201);
	resp.end();
});

app.delete("/cart", function(req, resp) {
	console.log(">> performing checkout");
	console.log("cart contents ", req.session.userdata.cart);
	req.session.destroy();
	resp.status(200);
	resp.end();
});

app.get("/cart/history", 
	function(req, resp, next) {
		if (!req.query.xxyyzz) {
			resp.redirect("https://google.com");
			return;
		}
		next();
	},
	function(req, resp) {
		resp.status(200);
		resp.type("application/json");
		resp.json({
			generatedOn: new Date(),
			history: req.session.userdata.history,
			lastVisit: req.session.userdata.lastVisit,
			cart: req.session.userdata.cart
		});
	}
);

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

app.use(express.static(path.join(__dirname, "../client")));

const port = 3000;

app.listen(port, function() {
	console.info("Application started on port %d", port);
	console.log("\tsession key = %s", secretKey);
});
