var power = function(b, e) {
	var r = 1;
	for (var i = 0; i < e; i++)
		r = r * b;
	return (r);
}

var result = power(2, 3);
console.log(">>> result = ", result);

var mkPower = function(e) {
	var power = function(b) {
		console.log("e = %d", e);
		var result = 1;
		for (var i = 0; i < e; i++)
			result = result * b;
		return (result);
	}
	return (power);
}


var square = mkPower(2);

result = square(6);
console.log(result);

var cube = mkPower(3);
result = cube(3) // 2^3

console.log(result);
