//Load the libs
const q = require("q");
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);
const express = require("express");

const sessionStore = new MySQLStore({
	host: "localhost", port: 3306,
	user: "fred", password: "fred",
	database: "derby_sample",
});

const pool = mysql.createPool({
	host: "localhost", port: 3306,
	user: "fred", password: "fred",
	database: "derby_sample",
	connectionLimit: 4
});
const sessionConfig = session({
	secret: "fsf17r3",
	resave: true,
	saveUninitialized: true,
	store: sessionStore,
	cookie: {
		maxAge: 15 * 60 * 1000 //15 mins in ms
	}
});

const mkQuery = function(sql, pool) {
	return (function() {
		const defer = q.defer();
		//Collect arguments
		const args = [];
		for (var i in arguments)
			args.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}

			conn.query(sql, args, function(err, result) {
				if (err) 
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});

		return (defer.promise);
	});
}

const SELECT_CUSTOMER_BY_NAME = 
		"select CUSTOMER_ID, NAME from CUSTOMER where NAME like ?";

const SELECT_PURCHASE_ORDER_BY_CUSTOMER_ID =
		"select * from PURCHASE_ORDER where CUSTOMER_ID = ?";

const findCustomerByName = mkQuery(SELECT_CUSTOMER_BY_NAME, pool);
const findPurchaseOrderByCustomerId = 
		mkQuery(SELECT_PURCHASE_ORDER_BY_CUSTOMER_ID, pool);

//Create an instance of the application
app = express();

app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());
app.use(sessionConfig);

app.use(function(req, resp, next) {

	if (!req.session.queryCache) {
		console.log("initializing new session");
		req.session.queryCache = {};
	}
	next();
});

/*
 * { search: "fred" }
 */
app.post("/api/customer", function(req, resp) {

	const searchTerm = "%" + req.body.search + "%";

	console.log("search term = %s", searchTerm);

	findCustomerByName(searchTerm)
		.then(function(result) { 
			//result is any arrary of records
			if (result.length > 0) {
				resp.status(200);
				resp.type("application/json");
				resp.json(result[0]);
			} else {
				resp.status(404);
				resp.end();
			}
		})

});

app.get("/api/purchaseorder/:term", function(req, resp) {

	const searchTerm = "%" + req.params.term + "%";

	console.log("---- req.session.queryCache ", req.session.queryCache);

	if (req.session.queryCache[searchTerm]) {
		console.log("Getting the result from cache");
		resp.status(200);
		resp.type("application/json")
		resp.json(req.session.queryCache[searchTerm].result);
		return;
	}

	findCustomerByName(searchTerm)
		.then(function(results) {
			if (results.length > 0) {
				const customer = results[0];
				return (customer);
			}
			return (q.reject("Customer not found"));
		})
		.then(function(customer) {
			findPurchaseOrderByCustomerId(customer.CUSTOMER_ID)
				.then(function(result) {
					customer.purchaseOrder = result;

					resp.status(200);
					resp.type("application/json")
					resp.json(customer);

					req.session.queryCache[searchTerm] = {
						result: customer, 
						timestamp: (new Date()).getTime()
					}
					console.log("added in to cache = ", 
						req.session.queryCache[searchTerm]);

					req.session.save();
				})
		})
		.catch(function(err) {
			console.log("rejected");
			resp.status(404);
			resp.end();
		});;
});

app.get("/api/customer/:cid/purchaseorder", function(req, resp) {
	// conceptually same as $stateParams.cid
	const custId = parseInt(req.params.cid);

	resp.type("application/json");

	findPurchaseOrderByCustomerId(custId)
		.then(function(result) {
			resp.status(200);
			resp.json(result);

		}).catch(function(err) {
			resp.status(500);
			resp.json(err);
		});
});

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));
app.use(express.static(path.join(__dirname, "../client")));

const port = process.env.APP_PORT || 3000;

app.listen(port, function() {
	console.log("Application started at %s on port %d"
			, new Date(), port);
});
