//load libs
const path = require("path");
const express = require("express");
const mysql = require("mysql");

//Create an instance of express
const app = express();

//Configure mysql pool
const pool = mysql.createPool({
    host: "localhost", port: 3306, //Server
    user: "fred", password: "fred", //login
    database: "sakila",
    connectionLimit: 4
});

const handleError = function(err, resp) {
    resp.status(500);
    resp.type("text/plain");
    resp.send(JSON.stringify(err));
}

const returnResult = function(records, resp) {
    resp.status(200);
    resp.type("text/html");
    var result = "<ul>";

    for (var idx in records) 
        result += "<li><h4>" + records[idx].title + "</h4><p>" 
            + records[idx].description + "</p></li>";

    result += "</ul>"
    resp.send(result);
}

const returnCount = function(records, resp) {
    console.info("returnCount: ", records);
    resp.status(200);
    resp.type("text/html");
    resp.send("<h1>Total films " + records[0].film_count + "</h1>");
}

const SQL_SELECT_FILMS = "select title, description from film limit 30";
const SQL_COUNT_FILMS = "select count(*) as film_count from film";

const makeQuery = function(queryStmt, pool) {
    //Closure
    return (function() {
        const p = new Promise(function(resolve, reject) {
            pool.getConnection(function(err, conn) {
                if (err) {
                    reject(err);
                    return;
                }
                params = []
                if (arguments.length > 0)
                    for (var i in arguments)
                        params.push(arguments[i]);

                conn.query(queryStmt, params ,function(err, result) {
                    if (err) {
                        reject(err); //-> .catch(function(err) { })
                        conn.release();
                        return;
                    }
                    resolve(result); //-> .then(function(result) { })
                    conn.release();
                });
            })
        });
        return (p);
    });
}

const selectFilms = makeQuery(SQL_SELECT_FILMS, conn);
const countFilms = makeQuery(SQL_COUNT_FILMS, conn);

//resource handlers
app.get("/films", function(req, resp) {
    selectFilms()
        .then(function(result) {

        }).catch(function(error) {

        })
    //Get a connection from the pool
    //Error or a connection object
    /*
    pool.getConnection(function(err, conn) {
        //Check if the pool has returned an error
        if (err) {
            console.error("Error: ", err);
            handleError(err, resp);
            return;
        }

        conn.query(SQL_SELECT_FILMS, function(err, result) {
            //Check if the query has resulted in an error
            if (err) {
                console.error("query error: ", err);
                handleError(err, resp);
                //Release the connection - otherwise will result in starvation
                conn.release();
                return;
            }

            returnResult(result, resp);

            conn.release();
        });
    })
    */
});

app.get("/films/count", function(req, resp) {
    pool.getConnection(function(err, conn) {
        //Check if the pool has returned an error
        if (err) {
            console.error("Error: ", err);
            handleError(err, resp);
            return;
        }

        conn.query(SQL_COUNT_FILMS, function(err, result){
            //Check if the query has resulted in an error
            if (err) {
                console.error("query error: ", err);
                handleError(err, resp);
                //Release the connection - otherwise will result in starvation
                conn.release();
                return;
            }

            returnCount(result, resp);

            conn.release();
        });

    })

})


app.use("/", express.static(path.join(__dirname, "../client")));

app.use("/libs", 
    express.static(path.join(__dirname, "../bower_components")));

//config the port
const port = process.env.APP_PORT || 3000;
//Start the server
app.listen(port, function() {
    console.info("Applicaton start at %s on port %d"
        , new Date(), port);
});