const path = require("path");
const express = require("express");

const app = express();

//Routes processing cycle
//
//
const mkLogger = function(prefix) {
	return (
		function(req, resp, next) {
			console.info(prefix + ": method: %s, resource: %s", req.method, req.originalUrl);
			next();
		}
	);
}


/*
app.use(function(req, resp, next) {

	console.info("Method: %s, Resource: %s", req.method, req.originalUrl);
	next();
}); */

app.use(mkLogger("ALL"));

app.get("/time", 

	mkLogger("TIME"), 

	function(req, resp) {
		resp.status(200);
		resp.type("text/html")
		resp.send("<h1>The current time is " + new Date() + "</h1>");
	}
);

app.use(

	mkLogger("STATIC"),

	express.static(path.join(__dirname, "../client")),

	function(req, resp, next) {
		console.log("We did not find %s in client", req.originalUrl);
		next();
	}
);

app.use(function(req, resp) {
	resp.status(404);
	resp.type("text/plain");
	resp.send("not found");
});


const port = 3000;

app.listen(port, function() {
	console.info("Application started on port %d", port);
});
